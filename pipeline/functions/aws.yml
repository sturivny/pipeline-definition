---
# gitlab-yaml-shellcheck: main=../../cki_pipeline.yml

.aws-function-definitions: |
    # Export AWS pipeline functions
    #
    # Bucket variables are URLs of the following format:
    # https://endpoint.url/name/path/
    #   BUCKET_<NAME>_AWS_ACCESS_KEY_ID
    #   BUCKET_<NAME>_AWS_SECRET_ACCESS_KEY

    # Output an eval-able list of variables for a bucket spec.
    # Args:
    #   $1: name of BUCKET variable in the form https://endpoint.url/name/path/
    function aws_vars {
        local scheme host bucket path bucket_ref access_key_ref secret_key_ref
        local proxy_scheme proxy_host proxy_ref
        IFS='/' read -r scheme _ host bucket path <<< "${!1}"
        echo 'declare -x AWS_S3_BUCKET="'"${bucket}"'"'
        # path prefix: ensure trailing slash if not empty
        echo 'declare -x AWS_S3_PATH="'"${path:+${path%/}/}"'"'
        bucket_ref="${bucket^^}"
        bucket_ref="${bucket_ref//[^a-zA-Z0-9]/_}"
        access_key_ref="BUCKET_${bucket_ref}_AWS_ACCESS_KEY_ID"
        secret_key_ref="BUCKET_${bucket_ref}_AWS_SECRET_ACCESS_KEY"
        proxy_ref="BUCKET_${bucket_ref}_PROXY"
        # access key: can be empty on AWS
        if [ -v "${access_key_ref}" ]; then
            echo 'declare -x AWS_ACCESS_KEY_ID="'"${!access_key_ref}"'"'
        fi
        # secret key: can be empty on AWS
        if [ -v "${secret_key_ref}" ]; then
            echo 'declare -x AWS_SECRET_ACCESS_KEY="'"${!secret_key_ref}"'"'
        fi
        if [ -v "${proxy_ref}" ]; then
            IFS='/' read -r proxy_scheme _ proxy_host _ <<< "${!proxy_ref}"
            echo 'declare -x AWS_S3_ENDPOINT_HOST="'"${proxy_host}"'"'
            echo 'declare -x AWS_S3_ENDPOINT_SCHEME="'"${proxy_scheme%:}"'"'
        else
            echo 'declare -x AWS_S3_ENDPOINT_HOST="'"${host}"'"'
            echo 'declare -x AWS_S3_ENDPOINT_SCHEME="'"${scheme%:}"'"'
        fi
        echo 'declare -xa AWS_S3_ENDPOINT_PARAMS=(--endpoint-url "'"${scheme}//${host}"'")'
    }

    # Download a file from an S3 bucket
    # Args:
    #   $1: name of BUCKET variable in the form SERVER|bucket|path/
    #   $2: source path relative to s3://bucket/path/
    #   $3: target file
    function aws_s3_download {
        eval "$(aws_vars "$1")"
        aws "${AWS_S3_ENDPOINT_PARAMS[@]}" s3 cp "s3://${AWS_S3_BUCKET}/${AWS_S3_PATH}$2" "$3"
    }

    # Sync a directory from an S3 bucket
    # Args:
    #   $1: name of BUCKET variable in the form SERVER|bucket|path/
    #   $2: source path relative to s3://bucket/path/
    #   $3: target directory
    function aws_s3_download_dir {
        eval "$(aws_vars "$1")"
        aws "${AWS_S3_ENDPOINT_PARAMS[@]}" s3 sync "s3://${AWS_S3_BUCKET}/${AWS_S3_PATH}$2" "$3" --size-only --only-show-errors
    }

    # Sync all artifacts from the artifacts S3 bucket
    function aws_s3_download_artifacts {
        local DEPENDENCIES DEPENDENCY PREVIOUS_JOBS LAST_JOB_ID
        if [ "${artifacts_mode}" != "s3" ]; then
            return
        fi

        echo_green "🗄️ Downloading artifacts from S3 bucket..."

        if [ ! -v ARTIFACT_DEPENDENCY ]; then
            echo_yellow "  No artifact dependency specified"
            return
        fi

        readarray -t DEPENDENCIES <<< "${ARTIFACT_DEPENDENCY}"
        for DEPENDENCY in "${DEPENDENCIES[@]}"; do
            echo_green "Downloading artifacts for '${CI_PIPELINE_ID}/${DEPENDENCY}'"
            readarray -t PREVIOUS_JOBS < <(
                aws_s3api_list_objects BUCKET_ARTIFACTS "${CI_PIPELINE_ID}/${DEPENDENCY}/" \
                    --delimiter '/'  \
                    --query 'CommonPrefixes[].{Prefix:Prefix}' \
                    --output text |
                    sed -r 's+.*/(.*)/+\1+')
            if [ "${PREVIOUS_JOBS[0]}" == "None" ]; then
                echo_yellow "  No previous job found"
                continue
            else
                echo "  Previous jobs: ${PREVIOUS_JOBS[*]}"
            fi
            LAST_JOB_ID="$(printf '%s\n' "${PREVIOUS_JOBS[@]}" | sort -n | tail -n 1)"

            aws_s3_download_artifact_from_pipeline_job "${CI_PIPELINE_ID}" "${DEPENDENCY}" "${LAST_JOB_ID}"
        done
    }

    function aws_s3_download_artifact_from_pipeline_job {
        local ARTIFACT_PIPELINE_ID=$1 ARTIFACT_JOB_NAME=$2 ARTIFACT_JOB_ID=$3 SOURCE_PATH JOB_CONTENTS

        SOURCE_PATH="${ARTIFACT_PIPELINE_ID}/${ARTIFACT_JOB_NAME}/${ARTIFACT_JOB_ID}"
        echo "Downloading artifacts from ${SOURCE_PATH}"
        JOB_CONTENTS="$(aws_s3api_list_objects BUCKET_ARTIFACTS "${SOURCE_PATH}/" --delimiter '/'  --query '[CommonPrefixes[].{Prefix:Prefix}, Contents[].{Prefix:Key}] | []' --output text)"
        # DEPRECATED: Old style with RC
        if grep -q '/rc$' <<< "${JOB_CONTENTS}"; then
            echo "Downloading rc file"
            loop aws_s3_download BUCKET_ARTIFACTS "${SOURCE_PATH}/rc" "${RC_FILE}"
        fi
        # New style with kcidb
        if grep -q '/kcidb_all.json$' <<< "${JOB_CONTENTS}"; then
            echo "Downloading kcidb_all.json file"
            loop aws_s3_download BUCKET_ARTIFACTS "${SOURCE_PATH}/${KCIDB_DUMPFILE_NAME}" "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"
        fi
        if grep -q '/artifacts/$' <<< "${JOB_CONTENTS}"; then
            echo "Downloading artifacts"
            loop aws_s3_download_dir BUCKET_ARTIFACTS "${SOURCE_PATH}/artifacts" "${ARTIFACTS_DIR}"
        fi
    }

    # Upload a file to an S3 bucket
    # Args:
    #   $1: name of BUCKET variable in the form SERVER|bucket|path/
    #   $2: source file
    #   $3: target path relative to s3://bucket/path/
    function aws_s3_upload {
        local bucket=$1 source=$2 target=$3
        shift 3
        eval "$(aws_vars "${bucket}")"
        aws "${AWS_S3_ENDPOINT_PARAMS[@]}" s3 cp "${source}" "s3://${AWS_S3_BUCKET}/${AWS_S3_PATH}${target}" "$@"
    }

    # Sync a directory to an S3 bucket
    # Args:
    #   $1: name of BUCKET variable in the form SERVER|bucket|path/
    #   $2: source directory
    #   $3: target path relative to s3://bucket/path/
    function aws_s3_upload_dir {
        eval "$(aws_vars "$1")"
        aws "${AWS_S3_ENDPOINT_PARAMS[@]}" s3 sync "$2" "s3://${AWS_S3_BUCKET}/${AWS_S3_PATH}$3" --size-only --only-show-errors
    }

    # Sync all artifacts to the artifacts S3 bucket
    function aws_s3_upload_artifacts {
        cd "${CI_PROJECT_DIR}"
        if [ -v ARTIFACTS ]; then
            echo "🗄️ Deleting artifacts that should not be archived..."
            rm -rf "${ARTIFACTS_DIR}${ARTIFACTS_TEMP_SUFFIX}"
            mkdir -p "${ARTIFACTS_DIR}${ARTIFACTS_TEMP_SUFFIX}"
            local ARTIFACT
            # intentionally no quoting for ARTIFACTS to get splitting and wildcard expansion
            for ARTIFACT in ${ARTIFACTS}; do
                if [ -e "${ARTIFACT}" ]; then
                    mv "${ARTIFACT}" "${ARTIFACTS_DIR}${ARTIFACTS_TEMP_SUFFIX}"
                fi
            done
            find "${ARTIFACTS_DIR}" -type f -exec rm -v {} +
            rm -rf "${ARTIFACTS_DIR}"
            mv "${ARTIFACTS_DIR}${ARTIFACTS_TEMP_SUFFIX}" "${ARTIFACTS_DIR}"
        fi

        echo "🗄️ Generating artifact meta data..."
        if [ "${artifacts_mode}" = "gitlab" ]; then
            jq \
                --arg mode "${artifacts_mode}" \
                '{"mode": $mode}'
        else
            jq \
                --arg mode "${artifacts_mode}" \
                --arg s3_url "$(aws_s3_url BUCKET_ARTIFACTS "${AWS_TARGET_PATH}")" \
                --arg s3_browse_url "$(aws_s3_browse_url BUCKET_ARTIFACTS "${AWS_TARGET_PATH}")" \
                --arg s3_index_url "$(aws_s3_url BUCKET_ARTIFACTS "${AWS_TARGET_PATH}/index.html")" \
                '{"mode": $mode, "s3_url": $s3_url, "s3_browse_url": $s3_browse_url, "s3_index_url": $s3_index_url}'
        fi > "${CI_PROJECT_DIR}/${ARTIFACTS_META_PATH}" <<< "null"

        if [ "${artifacts_mode}" = "gitlab" ]; then
            return
        fi

        echo "🗄️ Uploading artifacts to S3 bucket..."
        # DEPRECATED: Old style with RC
        if [ -f "${RC_FILE}" ]; then
            echo "Uploading rc file"
            loop aws_s3_upload BUCKET_ARTIFACTS "${RC_FILE}" "${AWS_TARGET_PATH}/rc" --content-type text/plain
        fi
        # New style with kcidb
        if [ -f "${KCIDB_DUMPFILE_NAME}" ]; then
            echo "Uploading ${KCIDB_DUMPFILE_NAME} file"
            loop aws_s3_upload BUCKET_ARTIFACTS "${KCIDB_DUMPFILE_NAME}" "${AWS_TARGET_PATH}/${KCIDB_DUMPFILE_NAME}"
        fi
        if [ -d "${ARTIFACTS_DIR}" ]; then
            echo "Uploading artifacts"
            loop aws_s3_upload_dir BUCKET_ARTIFACTS "${ARTIFACTS_DIR}" "${AWS_TARGET_PATH}/artifacts"
        fi

        echo "🗄️ Generating and uploading static artifact directory list..."
        aws_index_html BUCKET_ARTIFACTS "${AWS_TARGET_PATH}" > index.html
        loop aws_s3_upload BUCKET_ARTIFACTS index.html "${AWS_TARGET_PATH}/index.html"
        rm -f index.html

        if [ "${artifacts_mode}" = "s3" ]; then
            echo "Removing artifacts to prevent GitLab from archiving them"
            # allow the rc and kcidb files as a GitLab artifacts, but remove everything in artifacts/
            rm -rf "${ARTIFACTS_DIR}"
            mkdir -p "${ARTIFACTS_DIR}"
        fi

    }

    # Get a directory listing for an S3 bucket
    # Args:
    #   $1: name of BUCKET variable in the form SERVER|bucket|path/
    #   $2: path relative to s3://bucket/path/
    function aws_s3_ls {
        eval "$(aws_vars "$1")"
        aws "${AWS_S3_ENDPOINT_PARAMS[@]}" s3 ls "s3://${AWS_S3_BUCKET}/${AWS_S3_PATH}$2"
    }

    # List all objects in an S3 bucket
    # Args:
    #   $1: name of BUCKET variable in the form SERVER|bucket|path/
    #   $2: source path relative to s3://bucket/path/
    #   $@: other parameters to pass to aws s3api list-objects
    function aws_s3api_list_objects {
        eval "$(aws_vars "$1")"
        local PREFIX="$2"
        shift 2
        aws "${AWS_S3_ENDPOINT_PARAMS[@]}" s3api list-objects --bucket "${AWS_S3_BUCKET}" --prefix "${AWS_S3_PATH}${PREFIX}" "$@"
    }

    # Get the mime type of an object in an S3 bucket
    # Args:
    #   $1: name of BUCKET variable in the form SERVER|bucket|path/
    #   $2: source path relative to s3://bucket/path/
    function aws_s3api_content_type {
        local bucket=$1 source=$2
        shift 2
        eval "$(aws_vars "${bucket}")"
        aws "${AWS_S3_ENDPOINT_PARAMS[@]}" s3api head-object --bucket "${AWS_S3_BUCKET}" --key "${AWS_S3_PATH}${source}" --query ContentType --output text
    }

    # Remove files from an S3 bucket
    # Args:
    #   $1: name of BUCKET variable in the form SERVER|bucket|path/
    #   $2: target path relative to s3://bucket/path/
    #   $@: other parameters to pass to aws s3 rm
    function aws_s3_rm {
        eval "$(aws_vars "$1")"
        local PREFIX="$2"
        shift 2
        aws "${AWS_S3_ENDPOINT_PARAMS[@]}" s3 rm "s3://${AWS_S3_BUCKET}/${AWS_S3_PATH}${PREFIX}" "$@"
    }

    # Output a curl-able URL for a public bucket
    # Args:
    #   $1: name of BUCKET variable in the form SERVER|bucket|path/
    #   $2: path relative to s3://bucket/path/
    function aws_s3_url {
        eval "$(aws_vars "$1")"
        local url="${AWS_S3_ENDPOINT_SCHEME}://${AWS_S3_ENDPOINT_HOST}/${AWS_S3_BUCKET}/${AWS_S3_PATH}$2"
        echo "${url// /%20}"
    }

    # Generate a bare-bones HTML index page for a bucket
    # Args:
    #   $1: name of BUCKET variable in the form SERVER|bucket|path/
    #   $2: path relative to s3://bucket/path/
    function aws_index_html {
        if [ "${artifacts_mode}" != "gitlab" ]; then
            eval "$(aws_vars "$1")"
            echo "<!doctype html><title>${AWS_S3_BUCKET}/${AWS_S3_PATH}$2</title>"
            aws_s3api_list_objects BUCKET_ARTIFACTS "$2/" \
                | jq -r --arg bucket "${AWS_S3_BUCKET}" \
                  '.Contents[].Key | ("<a href=\"/" + $bucket + "/" + (. | @uri) + "\">" + . + "</a><br>")'
        fi
    }

    # Output a browsable URL for a public bucket
    # Args:
    #   $1: name of BUCKET variable in the form SERVER|bucket|path/
    #   $2: path relative to s3://bucket/path/
    function aws_s3_browse_url {
        eval "$(aws_vars "$1")"
        local url="${AWS_S3_ENDPOINT_SCHEME}://${AWS_S3_ENDPOINT_HOST}/${AWS_S3_BUCKET}/index.html?prefix=${AWS_S3_PATH}$2"
        echo "${url// /%20}"
    }

    # Output a curl-able URL for an artifact
    # Args:
    #   $1: name of artifact relative to ${CI_PROJECT_DIR}
    function artifact_url {
        if [ "${artifacts_mode}" = "s3" ]; then
            aws_s3_url BUCKET_ARTIFACTS "${AWS_TARGET_PATH}/$1"
        else
            echo "${CI_API_JOB_URL}/artifacts/$1"
        fi
    }
    # Output a browsable URL for an artifact
    # Args:
    #   $1: name of artifact relative to ${CI_PROJECT_DIR}
    function browsable_artifact_url {
        if [ "${artifacts_mode}" = "s3" ]; then
            aws_s3_url BUCKET_ARTIFACTS "${AWS_TARGET_PATH}/$1"
        else
            echo "${CI_JOB_URL}/artifacts/file/$1"
        fi
    }

    # Output a browsable URL for an artifact directory
    # Args:
    #   $1: name of artifact directory relative to ${CI_PROJECT_DIR}
    function browsable_artifact_directory_url {
        if [ "${artifacts_mode}" = "s3" ]; then
            aws_s3_browse_url BUCKET_ARTIFACTS "${AWS_TARGET_PATH}/$1"
        else
            echo "${CI_JOB_URL}/artifacts/browse/$1"
        fi
    }
