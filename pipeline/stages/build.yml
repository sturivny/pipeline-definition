---
# gitlab-yaml-shellcheck: main=../../cki_pipeline.yml

.build-prepare: |
  # 🛠 Prepare to compile the kernel.
  kcidb_build set-time start_time now
  kcidb_build set comment "CKI build of ${name}"
  # Update the state file with architecture details and package name.
  set_kernel_architecture_debug
  rc_state_set package_name "${package_name}"
  kcidb_build set misc/package_name "${package_name}"
  if [[ "$(arch)" != "x86_64" ]]; then
    echo "ERROR: Unsupported build machine architecture ($(arch)) for the build stage."
    echo "ERROR: Only 'x86_64' architecture is currently supported as the builder for the build stage"
    exit 1
  fi
  # Save the compiler version
  if [[ "${compiler}" == 'clang' ]] ; then
    # clang does not use cross compile prefixes
    rc_state_set compiler "$(clang --version | head -1)"
    kcidb_build set compiler "$(clang --version | head -1)"
  else  # default, no other values supported
    rc_state_set compiler "$("${CROSS_COMPILE:-}"gcc --version | head -1)"
    kcidb_build set compiler "$("${CROSS_COMPILE:-}"gcc --version | head -1)"
  fi
  # Mark the build stage as failed for now and replace it with a successful
  # status if we make it all the way through the stage later.
  rc_state_set stage_build fail
  rc_state_set buildlog "${BUILDLOG_PATH}"
  rc_state_set buildlog_url "$(artifact_url "${BUILDLOG_PATH}")"
  kcidb_build set log_url "$(artifact_url "${BUILDLOG_PATH}")"
  # Set the path to the ccache tarball stored in S3.
  CCACHE_FILENAME="${name}-${CI_JOB_NAME##* }.tar"
  # Create a directory for fixing up kernels
  mkdir -p "${KERNEL_CLEANUP_DIR}"

.build-prepare-rpm: |
  # 🛠  Prepare directories to hold binaries and set up macros
  if [[ "${make_target}" = 'rpm' ]]; then
    # Make a directory to hold the RPMs.
    mkdir -p "${WORKDIR}/rpms/${ARCH_CONFIG}/"
    # Set up RPM macros for this build.
    echo "%_rpmdir ${WORKDIR}/rpms" | tee -a ~/.rpmmacros

    # In RHEL9 /tmp is a tmpfs and it's removed in every reboot.
    # To use gcov we need to change the default rpmbuild path
    # ("/tmp/rpmbuild")
    if is_true "${coverage}"; then
      echo "%_topdir /cki/rpmbuild" | tee -a ~/.rpmmacros
    fi

    # Override dist tag if specified
    if [ -n "${disttag_override}" ] ; then
      echo_yellow "Manual disttag override requested: ${disttag_override}"
      echo "%dist ${disttag_override}" | tee -a ~/.rpmmacros
    fi
  fi

.build-prepare-tarball: |
  # 🗒 Extract kernel source and get configuration file for tarballs.
  if [[ ${make_target} = 'targz-pkg' ]]; then
    if [[ "${compiler}" == 'clang' ]] ; then
      if [[ "${ARCH_CONFIG}" == "s390x" ]]; then
        # lld is not supported with s390x: https://github.com/ClangBuiltLinux/linux/issues/1524
        MAKE=(make "CC=clang")
      else
        MAKE=(make "LLVM=1")
      fi
    else
      MAKE=(make)
    fi

    tar -C "${WORKDIR}/" -xzf "${KERNEL_TARGZ_PATH}"
    cd "${WORKDIR}"
      echo_yellow "Generating kernel configuration..."

      # Use curl with aws_s3_url as aws_s3_download does not support --no-sign-request
      # to download without credentials
      curl --output .config "$(aws_s3_url BUCKET_LOOKASIDE_KERNEL_CONFIGS "${ARCH_CONFIG}.config")"
      # We run 'make config_target' (defaults to olddefconfig) just in case new kernel
      # options exist that are missing from the downloaded config.
      "${MAKE[@]}" "${config_target}" 2>&1 | tee -a "${CI_PROJECT_DIR}/${BUILDLOG_PATH}"

      # Handle kernel oops as kernel panic, that should cause the machine to reboot
      # and next tests to run in more stable environment.
      scripts/config --enable PANIC_ON_OOPS 2>&1 | tee -a "${CI_PROJECT_DIR}/${BUILDLOG_PATH}"

      if is_true "${build_selftests}" && is_true "${DEBUG_KERNEL}"; then
        # Enable all config options required by selftests
        "${MAKE[@]}" kselftest-merge 2>&1 | tee -a "${CI_PROJECT_DIR}/${BUILDLOG_PATH}"
        # increase CONFIG_LOCKDEP_BITS: https://datawarehouse.internal.cki-project.org/issue/473
        scripts/config --set-val CONFIG_LOCKDEP_BITS 16 2>&1 | tee -a "${CI_PROJECT_DIR}/${BUILDLOG_PATH}"
        # increase MAX_LOCKDEP_CHAINS: https://datawarehouse.internal.cki-project.org/issue/468
        scripts/config --set-val CONFIG_LOCKDEP_CHAINS_BITS 17 2>&1 | tee -a "${CI_PROJECT_DIR}/${BUILDLOG_PATH}"
        # NOTE the config_target variable differs from the trigger variable from now
        # on in the build stage!
        config_target="${config_target} + kselftest-merge"
      else
        # Disable CONFIG_DEBUG_INFO to conserve resources and time unless building
        # selftests, as BPF selftests require this config.
        scripts/config --disable DEBUG_INFO 2>&1 | tee -a "${CI_PROJECT_DIR}/${BUILDLOG_PATH}"
      fi

      # Enable realtime preemption if this is a realtime kernel tree
      if is_true "${rt_kernel}" ; then
        scripts/config --enable PREEMPT_RT_FULL 2>&1 | tee -a "${CI_PROJECT_DIR}/${BUILDLOG_PATH}"
      fi
    cd "${CI_PROJECT_DIR}"

    # Always save the config file to easily reproduce failures
    CONFIG_FILE=artifacts/kernel-${name}-${KCIDB_BUILD_ID//:/_}.config
    cp "${WORKDIR}/.config" "${CI_PROJECT_DIR}/${CONFIG_FILE}"
    # Set the config related rc file variables already in case of build failure
    rc_state_set kernel_config_url "$(artifact_url "${CONFIG_FILE}")"
    kcidb_build set config_name "${config_target}"
    kcidb_build set config_url "$(artifact_url "${CONFIG_FILE}")"
    echo_green "Generated kernel configuration available as ${CONFIG_FILE} in the job artifacts."

    # Remove the source tarball as it is no longer needed.
    rm -fv "${KERNEL_TARGZ_PATH}"
  fi

.build-restore-ccache: |
  # Restore the ccache objects from S3.
  function restore_ccache() {
    # NOTE(mhayden): The primary group inside the container is 0, so tar thinks
    # it is root, so it tries to restore file ownership and modes, which
    # fails. The extra tar arguments here ensures that tar ignores all of those
    # extra bits inside the tarball.
    aws_s3_download BUCKET_CCACHE "${CCACHE_FILENAME}" - | \
      tar --no-same-owner --no-same-permissions --no-xattrs --touch -xf -
  }
  mkdir -p "${CCACHE_DIR}"
  # Restore the ccache from a previous build (up to 5 attempts).
  cd "${CCACHE_DIR}"
    echo "Restoring ccache..."
    for _ in {1..5}; do
      if restore_ccache; then
        break;
      else
        # Delete a partial restore if there was a network problem during
        # download.
        rm -rf -- *
      fi
    done
    ccache -sz
  cd "${CI_PROJECT_DIR}"

.build-print-variables: |
  # Print build environment variables for local reproducer runs.
  echo_yellow "Exported variables:"
  echo_yellow "    ARCH=${ARCH}"
  if [ -n "${CROSS_COMPILE:-}" ] ; then
    echo_yellow "    CROSS_COMPILE=${CROSS_COMPILE}"
  fi

.build-compile-tarball: |
  # 📥 Compile the kernel into a tarball.
  if [[ ${make_target} = 'targz-pkg' ]]; then
    # Compile the kernel.
    cd "${WORKDIR}"
      export KERNEL_RELEASE
      KERNEL_RELEASE="$("${MAKE[@]}" -s kernelrelease 2>/dev/null | tail -n1)"
      rc_state_set kernel_version "${KERNEL_RELEASE}"
      kcidb_checkout set misc/kernel_version "${KERNEL_RELEASE}"

      if is_true "${DEBUG_KERNEL}"; then
        # don't remove debug symbols from modules,
        # this information might be necessary when decoding call traces
        MAKE_CMD="${MAKE[*]} -j${MAKE_JOBS} targz-pkg"
      else
        MAKE_CMD="${MAKE[*]} -j${MAKE_JOBS} INSTALL_MOD_STRIP=1 targz-pkg"
      fi
      rc_state_set make_opts "${MAKE_CMD}"
      kcidb_build set command "${MAKE_CMD}"
      echo_yellow "Compiling the kernel with: ${MAKE_CMD}"

      # Save timestamp before starting the build
      store_time
      ${MAKE_CMD} 2>&1 | ts -s | tee -a "${CI_PROJECT_DIR}/${BUILDLOG_PATH}" | tail -n 25
      # Calculate and save build duration after ending the build
      rc_state_set build_time "$(calculate_duration)"
      kcidb_build set-int duration "$(calculate_duration)"
      export TARBALL_FILE=artifacts/kernel-${name}-${KCIDB_BUILD_ID//:/_}.tar.gz
      mv linux-*.tar.gz "${CI_PROJECT_DIR}/${TARBALL_FILE}"

      echo_green "Kernel compiled successfully, binary available as ${TARBALL_FILE} in the job artifacts."

      # Build selftests if configured. Only build for x86_64 with selftest configs:
      # - We don't have native libs needed as we use a single root and installing
      #   multiple same ones leads to overwrites; so we can't build selftests on
      #   non-x86_64 architectures
      # - selftest configs pull in debug options and we don't want to mess with the
      #   regular build and only want to build selftests for the branch that has the
      #   debug configs
      if is_true "${build_selftests}" && is_true "${DEBUG_KERNEL}"; then
        echo_yellow "Building kernel selftests..."

        rc_state_set selftests_buildlog "${SELFTESTS_LOG_PATH}"
        rc_state_set selftests_buildlog_url "$(artifact_url "${SELFTESTS_LOG_PATH}")"
        rc_state_set selftest_subsets_retcodes "${SELFTESTS_BUILD_RESULTS_PATH}"
        rc_state_set selftest_subsets_retcodes_url "$(artifact_url "${SELFTESTS_BUILD_RESULTS_PATH}")"

        if [[ -n "${selftest_subsets:-}" ]] ; then  # build only specific selftests subsets
          for subset in ${selftest_subsets} ; do
            # Save actual retcode from make command for each subset. We don't want the pipeline
            # to fail/abort when selftests builds fail, so we have to work around our -e and
            # pipefail settings via {} || true
            { make -C tools/testing/selftests SKIP_TARGETS="" TARGETS="${subset}" FORCE_TARGETS=1 2>&1 | ts -s | \
                      tee -a "${CI_PROJECT_DIR}/${SELFTESTS_LOG_PATH}" | tail -n 25 ;
              make_retcode=${PIPESTATUS[0]}
              echo "${subset}: ${make_retcode}" >> "${CI_PROJECT_DIR}/${SELFTESTS_BUILD_RESULTS_PATH}" ; } || true
              kcidb_add_selftest "${subset}" "${make_retcode}"
          done

          # Check which subsets actually built so we can skip packaging the broken ones. That way
          # we can actually run at least some of them to get some test coverage.
          gen_target="$(get_successful_selftests "${CI_PROJECT_DIR}/${SELFTESTS_BUILD_RESULTS_PATH}")"

        else  # build all selftests
          make -C tools/testing/selftests SKIP_TARGETS="" 2>&1 | ts -s | tee -a "${CI_PROJECT_DIR}/${SELFTESTS_LOG_PATH}" | tail -n 25
          make_retcode=$?
          echo "all: ${make_retcode}" >> "${CI_PROJECT_DIR}/${SELFTESTS_BUILD_RESULTS_PATH}"
          kcidb_add_selftest "all" "${make_retcode}"
        fi

        # Save the retcode, not just logs. Use {} || true to avoid aborting the pipeline.
        { make -C tools/testing/selftests gen_tar SKIP_TARGETS="" "${gen_target:-}" 2>&1 | ts -s | \
                  tee -a "${CI_PROJECT_DIR}/${SELFTESTS_LOG_PATH}" | tail -n 25 ;
          make_retcode=${PIPESTATUS[0]}
          echo "install and packaging: ${make_retcode}" >> "${CI_PROJECT_DIR}/${SELFTESTS_BUILD_RESULTS_PATH}" ;
          kcidb_add_selftest "install and packaging" "${make_retcode}"
          mv "tools/testing/selftests/kselftest_install/kselftest-packages/${SELFTESTS_TARGZ_PATH##*/}" \
             "${CI_PROJECT_DIR}/${SELFTESTS_TARGZ_PATH}" ; } || true

        # Only set the values if the commands actually succeeded
        if [[ -f ${CI_PROJECT_DIR}/${SELFTESTS_TARGZ_PATH} ]] ; then
          echo_yellow "At least some selftest subset builds succeeded. See full status and return codes from 'make' below:"
          cat "${CI_PROJECT_DIR}/${SELFTESTS_BUILD_RESULTS_PATH}"
        else
          echo_red "Selftests build failed, check out $(artifact_url "${SELFTESTS_LOG_PATH}") for more details."
        fi

      fi
    cd "${CI_PROJECT_DIR}"

    cd "${KERNEL_CLEANUP_DIR}"
      tar -xf "${CI_PROJECT_DIR}/${TARBALL_FILE}"

      # Fixup /source and /build symlinks. These are broken because tarball building expects running
      # on the same machine as the kernels are built which is not our case. RPM packages already handle
      # this situation.
      rm -f "lib/modules/${KERNEL_RELEASE}/build"
      rm -f "lib/modules/${KERNEL_RELEASE}/source"
      mkdir -p "usr/src/kernels/${KERNEL_RELEASE}"
      # Use excludes from mkspec. Also add "tar-install" directory to them as we don't need it.
      # FIXME Make EXCLUDES an array, and use either ${EXCLUDES[*]} or ${EXCLUDES[@]} as appropriate.
      EXCLUDES="*vmlinux* *.mod *.o *.ko *.cmd Documentation .config.old .missing-syscalls.d *.s tar-install"
      # shellcheck disable=SC2086 # FIXME Disabled on purpose as we want the splits of the words in the variable. It can be removed once join_by_multi uses arrays.
      EXCLUDES="$(join_by_multi ' --exclude=' ${EXCLUDES})"
      # Explicitly keep Documentation/Kconfig as it's needed for building modules,
      # and include/asm-generic/vmlinux.lds.h which is a >=v5.11 addition. We do want
      # to add the vmlinux.lds.h here instead of excluding all possible vmlinux binaries
      # in the excludes line as we could miss some which we don't want to happen.
      # shellcheck disable=SC2086 # FIXME Disabled on purpose as we want the spaces in ${EXCLUDES}; can be removed once join_by_multi uses arrays.
      tar cf - -C "${WORKDIR}" Documentation/Kconfig include/asm-generic/vmlinux.lds.h ${EXCLUDES} . | tar xf - -C "usr/src/kernels/${KERNEL_RELEASE}"
      ln -sf "/usr/src/kernels/${KERNEL_RELEASE}" "lib/modules/${KERNEL_RELEASE}/build"
      ln -sf "/usr/src/kernels/${KERNEL_RELEASE}" "lib/modules/${KERNEL_RELEASE}/source"

      # Fixes for cross compilation - remove broken parts
      rm -f "usr/src/kernels/${KERNEL_RELEASE}/scripts/basic/fixdep"
      rm -f "usr/src/kernels/${KERNEL_RELEASE}/scripts/mod/modpost"
      rm -f "usr/src/kernels/${KERNEL_RELEASE}/tools/bpf/resolve_btfids/resolve_btfids"
      rm -f "usr/src/kernels/${KERNEL_RELEASE}/tools/bpf/resolve_btfids/fixdep"
      rm -f "usr/src/kernels/${KERNEL_RELEASE}/tools/bpf/resolve_btfids/libbpf.a"
      rm -f "usr/src/kernels/${KERNEL_RELEASE}/tools/bpf/resolve_btfids/libbpf/libbpf.a"

      # Move uncompressed binary away from /boot if possible to not take up space
      if [ -f "boot/vmlinuz-${KERNEL_RELEASE}" ] || [ -f "boot/vmlinux-kbuild-${KERNEL_RELEASE}" ] ; then
        mv "boot/vmlinux-${KERNEL_RELEASE}" "lib/modules/${KERNEL_RELEASE}/"
      fi

      # And finally, put the tarball back together!
      tar --owner=root --group=root -zcf "${CI_PROJECT_DIR}/${TARBALL_FILE}" boot lib usr
    cd "${CI_PROJECT_DIR}"

    # Save information about a successful build
    rc_state_set stage_build pass
    rc_state_set kernel_package_url "$(artifact_url "${TARBALL_FILE}")"
  fi

.build-compile-rpm: |
  # 📦 Compile the kernel into an RPM.
  # Only do this step if we are building RPMs.
  if [[ ${make_target} = 'rpm' ]]; then
    # Save the kernel config before building in case there's an error.
    artifact_config_from_srpm "${ARTIFACTS_DIR}"/*.src.rpm "${package_name}" ""

    RPMBUILD_WITH_array=()
    RPMBUILD_WITHOUT_array=()
    create_array_from_string RPMBUILD_WITH
    create_array_from_string RPMBUILD_WITHOUT
    # Set up the rpmbuild command arguments and write them to the rc file.
    # Override the defaults for x86_64 as we can build more things here.
    if [[ "$(kcidb_build get architecture)" == "x86_64" ]] ; then
        RPMBUILD_WITHOUT_array=("trace")
        remove_from_array RPMBUILD_WITH_array cross
        # Enable debug config if we're running the debug build but not on regular x86_64 job!
        if is_true "${DEBUG_KERNEL}"; then
          RPMBUILD_WITH_array+=(debug)
          RPMBUILD_WITHOUT_array+=(up)
        else
          RPMBUILD_WITHOUT_array+=(debug)
        fi
    fi

    # Force the release kernel configuration if an override to the defaults is required.
    if is_true "${with_release}" ; then
      RPMBUILD_WITH_array+=(release)
    fi

    if [[ "${compiler}" == 'clang' ]] ; then
      RPMBUILD_WITH_array+=(toolchain_clang)
    fi

    if is_true "${coverage}"; then
      RPMBUILD_WITH_array+=(gcov)
    fi

    if is_true "${clang_lto}"; then
      RPMBUILD_WITH_array+=(clang_lto)
    fi

    rpmbuild_args=(--target "${ARCH_CONFIG}")
    for elt in "${RPMBUILD_WITH_array[@]+"${RPMBUILD_WITH_array[@]}"}"; do
        rpmbuild_args+=(--with "${elt}")
    done
    for elt in "${RPMBUILD_WITHOUT_array[@]+"${RPMBUILD_WITHOUT_array[@]}"}"; do
        rpmbuild_args+=(--without "${elt}")
    done
    # Write our make options to the rc file.
    rc_state_set make_opts "rpmbuild ${rpmbuild_args[*]}"
    kcidb_build set command "rpmbuild ${rpmbuild_args[*]}"
    echo_yellow "Compiling the kernel with: rpmbuild --rebuild ${rpmbuild_args[*]}"

    # Save timestamp before starting the build
    store_time
    # Build the arch-specific kernel RPMs.
    rpmbuild --rebuild "${rpmbuild_args[@]}" \
      artifacts/*.src.rpm 2>&1 | ts -s | tee -a "${CI_PROJECT_DIR}/${BUILDLOG_PATH}" | tail -n 25

    echo_yellow "Building noarch packages"
    rpmbuild --rebuild --target noarch artifacts/*.src.rpm 2>&1 | ts -s | tee -a "${CI_PROJECT_DIR}/${BUILDLOG_PATH}" | tail -n 25

    # Calculate and save build duration after ending the build
    build_time="$(($(calculate_duration) + $(kcidb_build get duration)))"
    rc_state_set build_time "${build_time}"
    kcidb_build set-int duration "${build_time}"

    # Artifact all RPMs so we can create the repo later
    mv "${WORKDIR}/rpms/"*/*.rpm "${ARTIFACTS_DIR}/"

    # Drop the kernel metapackage for debug builds so we can correctly configure the boot test excludes
    if is_true "${DEBUG_KERNEL}"; then
      kernel_version="$(kcidb_checkout get misc/kernel_version)"
      rm -f "${ARTIFACTS_DIR}/${package_name}-${kernel_version}.${ARCH_CONFIG}.rpm"
    fi

    rc_state_set stage_build pass
    echo_green "Kernel compilation succeeded. Check the publish stage for full yum/dnf repository."
  fi

.build-save-ccache: |
  # Save the ccache objects to S3.
  function store_ccache() {
    tar cf - . | aws_s3_upload BUCKET_CCACHE - "${CCACHE_FILENAME}"
  }
  # NOTE(mhayden): This hack is required because tar thinks that it is root
  # inside the container and it preserves ownership/modes/etc. When it tries
  # to restore with files that have the SUID bit set, it fails. The recursive
  # chmod here is gross, but it fixes the problem completely.
  chmod -R 0777 "${CCACHE_DIR}"
  # Store the ccache for a later build (up to 5 attempts).
  cd "${CCACHE_DIR}"
    ccache -s
    echo "Storing ccache..."
    for _ in {1..5}; do
      if store_ccache; then break; fi
    done
  cd "${CI_PROJECT_DIR}"

.build-tag-tests-expected: |
  # Populate field in misc to let DataWarehouse know we will test this build.
  if ! is_true "${skip_test}"; then
    kcidb_build set-bool misc/test_plan_missing true
  fi

.build:
  extends: [.with_artifacts, .with_retries, .with_builder_image]
  stage: build
  timeout: 5h
  variables:
    ARTIFACTS: "artifacts/*.rpm artifacts/*.config artifacts/*.tar.* ${BUILDLOG_PATH} ${SELFTESTS_LOG_PATH} ${SELFTESTS_TARGZ_PATH} ${SELFTESTS_BUILD_RESULTS_PATH} ${MR_DIFF_PATH}"
    ARTIFACT_DEPENDENCY: |-
      prepare builder
      merge
  dependencies:
    - prepare builder
    - merge
  before_script:
    - !reference [.common-before-script]
  script:
    - |
      kcidb_build append-dict misc/provenance --add-environment function="executor" url="${CI_JOB_URL}" service_name="gitlab"
    - !reference [.build-prepare]
    - !reference [.build-prepare-tarball]
    - !reference [.build-prepare-rpm]
    - !reference [.build-restore-ccache]
    - !reference [.build-print-variables]
    - !reference [.build-compile-tarball]
    - !reference [.build-compile-rpm]
    - !reference [.build-save-ccache]
    - !reference [.build-tag-tests-expected]
    - aws_s3_upload_artifacts
  after_script:
    - !reference [.common-after-script-head]
    - |
      if [[ "${CI_JOB_STATUS}" == "failed" ]]; then
        kcidb_build set-bool valid false
        aws_s3_upload_artifacts
      fi
    - !reference [.common-after-script-tail]
  tags:
    - pipeline-build-runner
  rules:
    - !reference [.skip_without_stage]
    - !reference [.skip_without_source]
    - !reference [.skip_without_debug]
    - !reference [.skip_without_architecture]
    - when: on_success

build i686:
  extends: [.build, .i686_variables]

build x86_64:
  extends: [.build, .x86_64_variables]

build x86_64 debug:
  extends: [.build, .x86_64_variables, .debug_variables]

build ppc64le:
  extends: [.build, .ppc64le_variables]

build aarch64:
  extends: [.build, .aarch64_variables]

build ppc64:
  extends: [.build, .ppc64_variables]

build s390x:
  extends: [.build, .s390x_variables]
