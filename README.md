# pipeline-definition

YAMLs for CKI pipeline definition

# Pipeline linting and unit tests

There is a `gitlab-yaml-shellcheck` linter script in [cki-lib] that allows to
lint the shell code embedded in the pipeline. This script is also used in the
GitLab CI/CD pipeline.

To run the linting and testing locally, use

```bash
./lint.sh
./tests.sh
```

If necessary, run them in a container via

```bash
podman run --rm --workdir /code --volume .:/code \
    registry.gitlab.com/cki-project/cki-tools/cki-tools:production \
    ./lint.sh
podman run --rm --workdir /code --volume .:/code \
    quay.io/cki/builder-rawhide:latest \
    ./tests.sh
```

In GitLab CI/CD, the tests run on all builder container images. This can be
simulated locally by replacing `quay.io/cki/builder-rawhide` with the
appropriate builder image.

# Pipeline testing

The preferred way is to make an MR and ask `cki-ci-bot` to test the changes. It
is also possible to use `retrigger` from [cki-tools]. This is what `cki-bot`
uses on the background too. You need to have valid tokens to use this method.

See the documentation on [pipeline triggering] for more details.

[cki-lib]: https://gitlab.com/cki-project/cki-lib
[cki-tools]: https://gitlab.com/cki-project/cki-tools
[pipeline triggering]: https://cki-project.org/docs/background/pipeline-triggering/
